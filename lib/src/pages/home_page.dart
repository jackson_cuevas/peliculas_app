import 'package:flutter/material.dart';
import 'package:peliculas/src/pages/game_page.dart';
import 'package:peliculas/src/pages/movie_page.dart';
import 'package:peliculas/src/search/search_delegate.dart';

void main() => runApp(HomePage());

class HomePage extends StatelessWidget {
  final List<Tab> myTabs = <Tab>[
    Tab(icon: Icon(Icons.movie), text: 'Movies'),
    Tab(icon: Icon(Icons.gamepad), text: 'Games')
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: myTabs.length,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.amberAccent,
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.search, color: Colors.white),
                onPressed: () {
                  showSearch(context: context, delegate: DataSearch());
                },
              ),
            ],
            bottom: TabBar(tabs: myTabs),
          ),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[MoviePage(), GamePage()],
          ),
        ),
      ),
    );
  }
}
