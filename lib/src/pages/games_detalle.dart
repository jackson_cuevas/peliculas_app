import 'package:flutter/material.dart';
import 'package:peliculas/src/Models/game-model.dart';

class GameDetalle extends StatelessWidget {
  // const GameDetalle({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Game game = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppbar(game),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox(height: 10.0),
                _posterTitulo(context, game),
                // _sinopsis(),
                 _descripcion(context, game),
                // _crearCasting(game)
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _crearAppbar(Game game) {
    return SliverAppBar(
      elevation: 2.0,
      // backgroundColor: Colors.yellow,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          game.name,
          style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
        background: FadeInImage(
          placeholder: AssetImage('assets/img/pokeball.gif'),
          image: NetworkImage(game.getBackgroundImg()),
          fadeInDuration: Duration(milliseconds: 150),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _posterTitulo(BuildContext context, Game game) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(children: <Widget>[
        Hero(
          tag: game.uniqueId,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image(
              image: NetworkImage(game.getPosterImg()),
              height: 150.0,
            ),
          ),
        ),
        SizedBox(width: 20.0),
        Flexible(
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(game.name,
            style: Theme.of(context).textTheme.headline5,
            overflow: TextOverflow.ellipsis),
            SizedBox(height: 4.0),
            Row(children: <Widget>[
              Icon(Icons.star_border),
              Text(game.popularity.toString())
            ],
            )
          ],
          ),
        ),
      ]),
    );
  }

  Widget _descripcion(BuildContext context, Game game) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Text(
        game.summary,
        textAlign: TextAlign.justify,
      ),
    );
  }

}
