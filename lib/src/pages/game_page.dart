

import 'package:flutter/material.dart';
import 'package:peliculas/src/providers/peliculas_provider.dart';
import 'package:peliculas/src/widgets/grid_list_widget.dart';

class GamePage extends StatelessWidget {
  
  final peliculasProvider = new PeliculasProvider();

  @override
  Widget build(BuildContext context) {
    return Container(
          child:_gridList()
    );
  }


  Widget _gridList() {
    return Container(
      child: FutureBuilder(
        future: peliculasProvider.getEnCines(),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if (snapshot.hasData) {
            return GridList(peliculas: snapshot.data);
          } else {
            return Container(
              height: 400.0,
              child: Center(child: CircularProgressIndicator(),
              )
            );
          }
        },
        
        ),

    );
  }

}