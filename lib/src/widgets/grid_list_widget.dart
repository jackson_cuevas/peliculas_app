import 'package:flutter/material.dart';
import 'package:peliculas/src/Models/pelicula_model.dart';

class GridList extends StatelessWidget {
  final List<Pelicula> peliculas;
  GridList({@required this.peliculas});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView.count(
        crossAxisCount: 2,
        mainAxisSpacing: 1,
        crossAxisSpacing: 1,
        padding: const EdgeInsets.all(8),
        childAspectRatio: 1,
        children: List.generate(peliculas.length, (index) {
          return _GridPhotoItem(peliculas[index]);
        }),
      ),
    );
  }
}

class _GridTitleText extends StatelessWidget {
  const _GridTitleText(this.text);

  final String text;

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: AlignmentDirectional.centerStart,
      child: Text(text, style: Theme.of(context).textTheme.subtitle1),
    );
  }
}

class _GridPhotoItem extends StatelessWidget {
  final Pelicula pelicula;
  _GridPhotoItem(this.pelicula);

  @override
  Widget build(BuildContext context) {
    final Widget image = Material(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
      clipBehavior: Clip.antiAlias,
      child: FadeInImage(
        image: NetworkImage(pelicula.getPosterImg()),
        placeholder: AssetImage('assets/img/pokeball.gif'),
        fit: BoxFit.cover,
      ),
    );

    return GridTile(
      footer: Material(
        color: Colors.transparent,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(4)),
        ),
        clipBehavior: Clip.antiAlias,
        child: GridTileBar(
          backgroundColor: Colors.black45,
          title: _GridTitleText(pelicula.originalTitle),
          subtitle: _GridTitleText(pelicula.title),
        ),
      ),
      child: image,
    );
  }
}
