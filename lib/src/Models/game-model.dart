import 'package:peliculas/src/Models/game-cover.dart';
import 'package:peliculas/src/providers/games/cover_provider.dart';

class Games {
  List<Game> items = new List();

  Games();

  Games.fromJsomList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final game = new Game.fromJsonMap(item);
      items.add(game);
    }
  }
}

class Game {
  final coverProvider = new CoverProvider();
  String uniqueId;
  int id;
  //int category;
  int cover;
  int createdAt;
  //List<int> externalGames;
  String name;
  double popularity;
  //String slug;
  String summary;
  //int updatedAt;
  String url;
  //List<int> ageRatings;
  //int collection;
  //int firstReleaseDate;
  //List<int> genres;
  //List<int> involvedCompanies;
  //List<int> keywords;
  //List<int> platforms;
  double rating;
  //int ratingCount;
  //List<int> releaseDates;
  //List<int> similarGames;
  //List<int> tags;
  //double totalRating;
  //int totalRatingCount;
  //List<int> themes;
  //double aggregatedRating;
  //int aggregatedRatingCount;
  //List<int> gameModes;
  //int pulseCount;
  //List<int> screenshots;
  //List<int> videos;
  //List<int> websites;
  //int parentGame;

  Game({
    this.id,
    //this.category,
    this.cover,
    this.createdAt,
    //this.externalGames,
    this.name,
    this.popularity,
    //this.slug,
    this.summary,
    //this.updatedAt,
    this.url,
    //this.ageRatings,
    //this.collection,
    //this.firstReleaseDate,
    //this.genres,
    //this.involvedCompanies,
    //this.keywords,
    //this.platforms,
    //this.rating,
    //this.ratingCount,
    //this.releaseDates,
    //this.similarGames,
    //this.tags,
    //this.totalRating,
    //this.totalRatingCount,
    //this.themes,
    //this.aggregatedRating,
    //this.aggregatedRatingCount,
    //this.gameModes,
    //this.pulseCount,
    //this.screenshots,
    //this.videos,
    //this.websites,
    //this.parentGame,
  });

  Game.fromJsonMap(Map<String, dynamic> json) {
    id = json['id'];
    //category = json['category'];
    cover = json['cover'];
    createdAt = json['created_t'];
    //externalGames = json['external_games'];
    name = json['name'];
    popularity = json['popularity'];
    //slug = json['slug'];
    summary = json['summary'];
    //updatedAt = json['updated_at'];
    url = json['url'];
    //ageRatings = json['age_ratings'];
    //collection = json['collection'];
    //firstReleaseDate = json['first_release_date'];
    //genres = json['genres'];
    //involvedCompanies = json['involved_companies'].cast<dynamic>();
    //keywords = json['keywords'].cast<dynamic>();
    //platforms = json['platforms'].cast<dynamic>();
    rating = json['rating'];
    //ratingCount = json['rating_count'];
    //releaseDates = json['release_dates'].cast<dynamic>();
    //similarGames = json['similar_games'].cast<dynamic>();
    //tags = json['tags'].cast<dynamic>();
    //totalRating = json['total_rating'];
    //totalRatingCount = json['total_rating_count'];
    //themes = json['themes'].cast<dynamic>();
    //aggregatedRating = json['aggregated_rating'];
    //aggregatedRatingCount = json['aggregated_rating_count'];
    //gameModes = json['game_modes'].cast<dynamic>();
    //pulseCount = json['pulse_count'];
    //screenshots = json['screenshots'].cast<dynamic>();
    //videos = json['videos'].cast<dynamic>();
    //websites = json['websites'].cast<dynamic>();
    //parentGame = json['parent_game'];
  }

  getPosterImg() {
    final cover = coverProvider.getGameCover(69359);
     return cover;
  }

  getBackgroundImg() {
    if (cover == null) {
      return 'https://us.123rf.com/450wm/pavelstasevich/pavelstasevich1811/pavelstasevich181101065/112815953-stock-vector-no-image-available-icon-flat-vector.jpg?ver=6';
    } else {
      Future<List<Cover>> cover = coverProvider.getGameCover(id);

      return cover;
    }
  }
}
