
class Covers {
  List<Cover> items = new List();

  Covers();

  Covers.fromJsomList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final game = new Cover.fromJsonMap(item);
      items.add(game);
    }
  }
}
class Cover {
  int id;
  int game;
  int height;
  String url;
  int width;

  Cover({
    this.id,
    this.game,
    this.height,
    this.url,
    this.width,
  });

  Cover.fromJsonMap(Map<String, dynamic> json) {
    id = json['id'];
    game = json['cover'];
    height = json['height'];
    url = json['url'];
    width = json['width'];
  }
}



