import 'dart:convert';

import 'package:peliculas/src/Models/game-cover.dart';
import 'package:http/http.dart' as http;

class CoverProvider {
  String _apiKey = 'd2be0b62b4725ecff2ab3fb1acd51888';
  String _url = 'api-v3.igdb.com';
  String _fields = 'fields *;';

  Future<List<Cover>> getGameCover(int id) async {
    _fields = 'fields *; where game = $id;';

    final url = Uri.https(_url, 'covers');

    final resp = await http.post(url,
        headers: <String, String>{'user-key': _apiKey}, body: _fields);
        
    if (resp.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      final decodedData = json.decode(resp.body);
      
        final cover = new Covers.fromJsomList(decodedData);
        return cover.items;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
    // final decodedData = json.decode(resp.body);

    // final cover = new Cover.fromJsonMap(decodedData[0]);
    // return cover;
  }
}
