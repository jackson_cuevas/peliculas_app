import 'dart:async';
import 'dart:convert';
import 'package:peliculas/src/Models/game-model.dart';
import 'package:http/http.dart' as http;

class GamesProvider {
  String _apiKey = 'd2be0b62b4725ecff2ab3fb1acd51888';
  String _url = 'api-v3.igdb.com';
  String _fields = 'fields *;';

  final _gamesPopularesStream = StreamController<List<Game>>.broadcast();

  Function(List<Game>) get pupularesSink => _gamesPopularesStream.sink.add;

  Stream<List<Game>> get popularesStream => _gamesPopularesStream.stream;

  void disposeStream() {
    _gamesPopularesStream?.close();
  }

  Future<List<Game>> getGames() async {
    final url = Uri.https(_url, 'games');
    return await _procesarRespuesta(url);
  }


  Future<List<Game>> getPopulares() async {
    _fields = 'fields *; where rating > 75';

    final url = Uri.https(_url, 'covers');

    return await _procesarRespuesta(url);
  }

  Future<List<Game>> _procesarRespuesta(Uri url) async {
    final resp = await http.post(url,
        headers: <String, String>{'user-key': _apiKey}, body: _fields);

    final decodedData = json.decode(resp.body);

    final games = new Games.fromJsomList(decodedData);

    return games.items;
  }

}
